/*
Рекурсивный и не рекурсивный вариант задачи о «Ханойских башнях»	
 */
public class Task4 {

    public static void recursiveHanoi(int discsQuantity, String currentStick, String targetStick, String bufferStick) {
        if (discsQuantity > 0) {
            recursiveHanoi(discsQuantity - 1, currentStick, bufferStick, targetStick);
            System.out.println(currentStick + "==>" + targetStick);
            recursiveHanoi(discsQuantity - 1, bufferStick, targetStick, currentStick);
        }
    }
}


