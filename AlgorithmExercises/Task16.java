import java.util.Arrays;

/*
Вокруг 	считающего стоят N человек, один из которых назван первым, а остальные 	занумерованы против
часовой стрелки 	числами от 2 до N. Считающий ведет счет до K, начиная с первого. Счет продолжается
до следующего игрока (при этом выбывшие 	из круга не считаются). И так до тех пор, 	пока не останется
1 человек. Требуется 	определить начальный номер этого 	человека.
 */
public class Task16 {

    public static int gerRemainingPersonNumber(int numberOfPeople, int knockoutNumber) {
        int[] people = new int[numberOfPeople];
        Arrays.fill(people, 1);
        int peopleCounter = numberOfPeople;
        int result = -1;

        int i = 0;
        while (peopleCounter > 1) {
            int counter = knockoutNumber;

            while (counter > 1) {
                if (people[i % numberOfPeople] != -1) {
                    counter--;
                }
                i++;
            }
            while (people[i % numberOfPeople] == -1) {
                i++;
            }
            people[i % numberOfPeople] = -1;
            peopleCounter--;
        }

        for (int j = 0; j < numberOfPeople; j++) {
            if (people[j] == 1) {
                result = j;
            }
        }
        return result + 1;
    }
}
