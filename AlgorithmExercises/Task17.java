import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*
Дано 3n монет, среди которых одна фальшивая (немного тяжелее всех остальных).
Требуется с помощью чашечных весов без гирь ровно за N взвешиваний определить
номер фальшивой монеты. Пользователь 	вводит 0, если чаши весов уравновешены,
1 – если перевесила левая чаша, и 2 – если перевесила правая.
 */
public class Task17 {
    public static int detectFakeCoin(int numberOfWeighings) {

        List<Integer> left = new ArrayList<>();
        List<Integer> right = new ArrayList<>();
        List<Integer> buffer = new ArrayList<>();
        List<Integer> coins = new ArrayList<>();

        for (int i = 1; i < Math.pow(3, numberOfWeighings) + 1; i++) {
            coins.add(i);
        }

        while (numberOfWeighings > 0) {
            left.addAll(coins.subList(0, coins.size() / 3));
            right.addAll(coins.subList(coins.size() / 3, (coins.size() / 3) * 2));
            buffer.addAll(coins.subList((coins.size() / 3) * 2, coins.size()));
            int state = input();
            coins.clear();

            if (state == 1) {
                coins.addAll(left);
            } else if (state == 2) {
                coins.addAll(right);
            } else if (state == 0) {
                coins.addAll(buffer);
            } else {
                System.out.println("Input error!");
                input();
            }
            left.clear();
            right.clear();
            buffer.clear();
            numberOfWeighings--;
        }
        return coins.get(0);
    }

    private static int input() {
        System.out.println("Enter the state of the scales:");
        System.out.println("0 - equilibrium, 1 - left is heavier, 2 - right is heavier");
        System.out.print("Your choice: \n");
        return new Scanner(System.in).nextInt();
    }
}
