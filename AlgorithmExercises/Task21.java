/*
Прямоугольник, стороны которого выражены натуральными числами а и b, разделен на квадраты
размером 1х1. Найдите число квадратов, 	пересекаемых диагональю прямоугольника.
 */
public class Task21 {
    public static int findNumberOfSquares(int rectangleWidth, int rectangleHeight) {
        if (rectangleWidth < rectangleHeight) {
            int buffer = rectangleHeight;
            rectangleHeight = rectangleWidth;
            rectangleWidth = buffer;
        }

        int buffer = -1;
        int counter = 0;

        for (int i = 1; i < rectangleWidth; i++) {
            int crossedJoinPoints = rectangleHeight * i % rectangleWidth;
            int crossedSides = rectangleHeight * i / rectangleWidth;

            if (crossedJoinPoints != 0) {
                if (crossedSides == buffer)
                    counter++;
                else {
                    counter += 2;
                    buffer = crossedSides;
                }
            }
        }
        return counter;
    }
}
