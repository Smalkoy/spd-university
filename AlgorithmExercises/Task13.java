/*
От заданного 	прямоугольника каждый раз отрезается 	квадрат максимальной площади
(длины сторон фигур выражаются натуральными числами). Найдите число таких квадратов.
 */
public class Task13 {
    private static int squareCounter = 0;

    public static int squaresInRectangle(int rectangleHeight, int rectangleWidth) {
        if (rectangleHeight == 0 || rectangleWidth == 0){
            return squareCounter++;
        }
        if (rectangleHeight < rectangleWidth) {
            squaresInRectangle(rectangleWidth - rectangleHeight, rectangleHeight);
        } else squaresInRectangle(rectangleHeight - rectangleWidth, rectangleWidth);
        return squareCounter++;
    }
}
