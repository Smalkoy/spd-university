/*
Реализовать алгоритм ручного деления «в столбик» двух заданных натуральных чисел.
 */
public class Task22 {
    public static int divide(int dividend, int divider) {
        int current = 1;
        int answer = 0;

        if (divider > dividend)
            return 0;

        if (divider == dividend)
            return 1;

        while (divider <= dividend) {
            divider <<= 1;
            current <<= 1;
        }

        divider >>= 1;
        current >>= 1;

        while (current != 0) {
            if (dividend >= divider) {
                dividend -= divider;
                answer |= current;
            }
            current >>= 1;
            divider >>= 1;
        }
        return answer;
    }
}
