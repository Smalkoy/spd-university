/*
 Задан массив  целых чисел. Определить максимальную длину (+ позиции начала и конца)
 последовательности идущих подряд различных элементов.
 */
public class Task1 {
    private int maxStart = 0;
    private int maxEnd = 0;
    private int maxLength = 0;

    public void printResult(int[] entrySequence) {
        int start = 0;
        int end = 0;
        int length = 0;

        for (int i = 0; i < entrySequence.length - 1; i++) {
            if (!(entrySequence[i] == entrySequence[i + 1] || i == entrySequence.length - 2)) {
                length++;
                end = i + 1;
            } else {
                if (length > maxLength) {
                    maxEnd = end;
                    maxStart = start;
                    maxLength = length;
                }
                start = i + 1;
                length = 1;
            }
        }
        System.out.println("maxLength = " + maxLength);
        System.out.println("Start = " + maxStart);
        System.out.println("End = " + maxEnd);
    }
}
