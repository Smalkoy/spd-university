/*
Есть строка, состоящая из слов. Необходимо реализовать наиболее эффективный алгоритм перестановки
слов в обратном порядке в предложении (без выделения дополнительной памяти).
 */

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Task2 {
    public static String reverseWords (String sentence) {
        String result = new String();
        List<String> words = Arrays.asList(sentence.split("[\\s\\p{Punct}]+"));
        Collections.reverse(words);
        for (String s : words)
        {
            result += s + "\t";
        }
        return result;
    }
}
